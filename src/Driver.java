public class Driver {

    private String name;
    private String address;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

/*
        Activity 1:

    Create a class called Driver with the following attributes:
    name - string
    age - int
    address - string
    add getters and setters for the name, age and address att
    In the main method of the Main class, create a new instance of the Driver class and store it in a variable called driver1.
    -Initialize values for the new driver object.
    in your zuitt-b180 folder Create a git repository named WDC043-S04
    Initialize a local git repository in your s4 folder, add the remote link and push to git with the commit message of Add activity code
    Link your online repository in boodle

 */

/*
    Classes have relationships:

    A Car could have a Driver. - "has relationship" - Composition - allows modelling objects to be able
 */