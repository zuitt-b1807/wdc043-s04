public class Car {

    //An object is an idea of a real world object.
    //A class is the code that describe the object.
    //An instance is a tangible copy of an idea instantiated or created from a class.
    //Attributes and Methods
    //public - the variable in the class is accessible anywhere in application.
    //Class attributes/properties must not be public, they should only be accessed and set via class methods called setters and getters
    //setters are public methods which will allow us set the value of the attribute an instance.
    //getters are public methods which will allow us to get the value of the attribute of an instance.
    private String make;
    private String brand;
    private int price;
    //Add a driver variable to add a property which is an object/instance of a Driver class.

    private Driver carDriver;

    //make getter
    public String getMake() {
        return make;
    }
    //parameters in Java needs its dataType declared
    public void setMake(String make) {
        //this keyword refers to the object where the constructor or setter is
        this.make = make;
    }

    public String getBrand() {
        return brand;
    }
    //To make a property read-only, don't include the setter method
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }


    //Methods are functions of an object which allows us to perform certain tasks
    //void - means that the function does not return anything. Because in Java, a 'function/methods' return dataType must be declared.
    public void start(){
        System.out.println("Vroom! Vroom!");
    }
    /*
        Create setters and getters for the brand and price property of our object.
        Update the brand and price of car1 in the Main class using the setters.
        Print the brand and price of car1 in the Main class using the getters.
    */

    //constructor is a method which allows us to set the initial values of an instance.
    //empty/default constructor - default constructor - allows us to create an instance with default initialized values for our properties.
//    public Car(){
        //empty or you can designate default values instead of getting them from parameters.
        //Java, actually already creates one for us, however, empty/default constructor made just by Java allows us to Java to set its own default values. If you create your own, you can your own default values.

//    }
    //parameterized constructor - allows us to initialize values to our attributes upon creation of the instance.
//    public Car(String make,String brand, int price){
//        this.make = make;
//        this.brand = brand;
//        this.price = price;
//    }


    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    public String getCarDriverName(){
        return this.carDriver.getName();
    }
}
